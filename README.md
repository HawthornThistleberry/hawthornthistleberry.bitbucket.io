# SolarEdge Monitor
## v1.7.5 2022-03-02
## by Frank J. Perricone (hawthorn@foobox.com)
#### many bits of code based on https://github.com/Jbithell/SolarEdgeESign
#### CORS proxying provided via https://grida.co/

A simple website that can be run on nearly any device to display almost-real-time data about your SolarEdge solar power system. Capable of running on a standalone tablet, in a PC browser tab, or just about anywhere else that has a reasonably modern browser. Features include:

- Displays current SolarEdge production and historical data at a glance
- Automatically updates on a schedule without page refreshes required
- Automatically stops updating SolarEdge data between 8:40pm and 4:20am to save API calls
- Displays different data at different rates to make the best use of SolarEdge's 300 calls per day
- Concise display shows current and historical information as well as environmental benefits
- Displays a graph of 1 to 30 days of power production data
- Can display in various zoom levels and with white or black background to adjust to most devices
- Can display current weather conditions in your location
- Can show monetary value of the energy your system generates
- Can integrate the status of a Sonnen battery system included within your minigrid

![Sample screen display](img/Sample.png)

# Installation

The simplest way to run this is to point a browser on your local device to https://hawthornthistleberry.bitbucket.io which will always show the current version of the software (though you'll need to refresh the page when it updates). You will simply need to add some parameters to the end of the URL to specify your location information, and any configuration items you want. More on this later.

Alternately, if you've downloaded this archive, you can install it locally on your devices, then open the `index.html` file in your browser. You'll end up with a URL that starts with `file://` but again you will have to add parameters to the end, just as if you ran it remotely.

This should run on nearly any device running a reasonably current browser. It's intended to run on a wall-mounted tablet, but no reason you can't have it on your PC, Mac, smartphone, tablet, Internet-enabled toaster, or cybernetic implant. (Note, though, if you run it on multiple devices at the same time you will need to use the `frequency` parameter to avoid exhausting your SolarEdge allowance; see the SolarEdge section below.)

# Adding Parameters

At a minimum you are going to have to add two parameters to the end of the URL to get this to work. There are many more you will be able to add. These will be explained in the sections on setting up SolarEdge, Weather, and Sonnen, as well as in the Parameters section of this document.

Parameters are added to the end of a URL in this format. The first one starts with a question mark, and all subsequent ones with an ampersand, and each one consists of a parameter name and the associated value or option, like this:

`https://hawthornthistleberry.bitbucket.io/?parameter1=option1`

`https://hawthornthistleberry.bitbucket.io/?parameter1=option1&parameter2=option2`

`https://hawthornthistleberry.bitbucket.io/?parameter1=option1&parameter2=option2&parameter3=option3`

For example, here's a bare bones version of what the URL would look likem in actual use, including only the SolarEdge required parameters:

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O`

And here's what it might look like if it's fully tricked out with many configuration options:

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O&value=19&graphDays=3&weatherKey=1n636n86o45p233qs98p5n23rqn1q58q&weatherCity=5233500&sonnen=192.168.1.15&frequency=10`

Once you have this just how you like it, bookmark it, or make your browser use it as its default home page, so you can get back to it.

Note that the order of parameters does not matter at all. But if you include the same parameter more than once, order might matter, and weird browser-specific things can happen, so don't do that.

# SolarEdge

To use this you must have a SolarEdge solar power cell system, as that's the meat-and-potatoes of what this is displaying. You will need to get an API key from your installer or SolarEdge; this is a long string of letters and numbers (e.g., `L4QLVQ1LOKCQX2193VSEICXW61NP6B1O`) which authorizes you to read information from the SolarEdge servers. You'll also need your site number, which is a six-digit number that you can see on your SolarEdge dashboard and which identifies your site. These are added to the URL using the `key` and `code` parameters, as shown here:

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O`

Note that the SolarEdge system only permits software to access their servers 300 times per day. This software, at the default frequency, is built to use up about 275 of those 300 calls, in order to optimize how up-to-the-minute the information you're seeing is. It does this by displaying different information at different rates:

- Current power production updates every five minutes (also includes the day, month, and lifetime amounts)
- Graph of past and current power production updates every fifteen minutes
- Environmental benefits updates every hour
- No updates happen between 8:40pm and 4:20am (or actually sunrise and sunset, if weather is enabled)

All this means if you want to run more than one copy of this, or you have any other devices using up your allowance of 300 calls a day, you will need to use the `frequency` parameter to reduce how frequently this monitors your SolarEdge. See more about this under SolarEdge Parameters below.

SolarEdge does sell batteries, but my system doesn't include a SolarEdge battery system, so this software doesn't have the ability to monitor them (yet). See the Sonnen Battery section below.

# Weather

By default the upper left icon on the display will be a static image of a solar panel array, but you can easily replace this with a dynamic view of your current weather. This is useful since weather has a huge effect on the production of solar power. You'll get an image and text showing current weather conditions, plus the current, low, and high temperature for the day.

To set this up, first, go to https://openweathermap.org/api and sign up for a free account. You will be sent, by email, an API key (this is similar in function and appearance to the SolarEdge API key, that's why it's called the same thing, but it's a different actual key).

Next, you need to find your nearest city supported by OpenWeatherMap, and specifically, find its city ID. You can specify the name and hope that works. If you happen to live in or near a city whose name is unique, worldwide, that may be good enough. Where I live, in Vermont, USA, hardly any city name isn't also found somewhere else in the world. OpenWeatherMap publishes its list of cities and their IDs in a somewhat user-unfriendly format: http://bulk.openweathermap.org/sample/city.list.json.gz which you'll need to unarchive, then read in a text editor in a computer-readable (but also just barely human-readable) format, to find your city, and even then, you'll need to compare the latitude and longitude to verify if you have the right one. If they would just add the ID to their map at https://openweathermap.org/weathermap this could be so much friendlier. Ah, well. On the bright side, you only do this once (until you move, at least).

Once you have your API key and city code, you can add these to the URL like this:

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O&weatherKey=1n636n86o45p233qs98p5n23rqn1q58q&weatherCity=5233500`

Tada! Weather should now appear, and update every five minutes. This also saves SolarEdge calls since the software now knows the actual time of dawn and dusk, and can use that to start and stop getting SolarEdge updates, rather than erring on the side of caution and starting at the earliest possible dawn and ending at the latest possible dusk. In winter in Vermont, this saves up to about 113 API calls per day!

# Sonnen Battery

My solar installer chose a Sonnen battery system instead of SolarEdge for my installation, so I built this software to optionally add Sonnen battery monitoring. Unlike with SolarEdge, the Sonnen battery doesn't have an API you query through its servers. Or maybe it does. Getting Sonnen to answer questions is nigh impossible. Heck, it took them almost ten months to replace my system when it failed.

There are two ways I have come up with to get Sonnen information from my battery into SolarMonitor. This process is ridiculously complicated and fraught with difficulties because the Sonnen only offers the information via an unencrypted web interface only visible inside your house network, and getting the information from there to SolarMonitor requires lots of web-plumbing and runs up against web-security considerations.

## The Pull Method

In a sane world, you would simply have SolarMonitor pull the information from the battery. However, this runs against not only a series of technical challenges, it hits some web security concerns that are nigh-insurmountable without additional software. To make this work:

1. You need a dynamic DNS service pointing to your home network, which usually also requires software on your home computer updating it when your home network's IP address changes.
2. You need port forwarding rules so incoming port-8080 requests are directed to the Sonnen battery.
3. Typically that means you need the Sonnen to have a MAC reservation so that the port forwarding can know where to find it.
4. But since SolarMonitor runs on a web service from this millennium (and thus on https) but the Sonnen only offers its data via unencrypted http, and since it'll be running across different domains, you need a way to get around the web security protocol called CORS. Solar Monitor is using the new bridged.cc CORS proxy and I'm hoping this one lasts! However, to use it, you will need to apply to them to get a CORS key at https://github.com/gridaco/base/issues/23 then follow up with them via Slack at https://join.slack.com/t/bridgedxyz/shared_invite/zt-nmf59381-prFEqq032K~aWe_zOekUmQ

To use this method, provide the web address of your home network as the `sonnen` parameter and the CORS key with the `cors` parameter. E.g.,

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O&weatherKey=1n636n86o45p233qs98p5n23rqn1q58q&weatherCity=5233500&sonnen=homeofsusanandrobin.dyndns.org`&cors=56rqp16r-orqr-7292-62np-24n84q22o827

## The Push Method

If having SolarMonitor reach into your network and pull the data out is problematic (and boy howdy it can be), how about pushing the data? This is where we start to get outside the scope of a core design concept of SolarMonitor: that you could run it on a single small device of any sort (if it has a web browser), without any server. But it does get around all the networking and CORS problems. The big downside, of course, is if the computer you're using to push it is shut down during a power outage, that's just when you'd be wanting current updates about the state of your Sonnen battery!

To do this, you need to set up a method that grabs the file from your battery on any schedule you like, and posts it to a Bitbucket repository you create under your own Bitbucket account. The repository has to be named sonnenstatus and must be (unfortunately) a public repository. The Sonnen data must be in a file named SonnenStatus.json. An example of how you might do this:

1. Install curl and git onto a computer.
2. Create the Bitbucket repository as described above and then clone it into a directory on that computer.
3. Have the computer regularly run in a batch script a set of commands which uses curl to save the data from your Sonnen directly into a file named SonnenStatus.json in your directory, then do a git commit and git push to push those updates to Bitbucket.

To use this method, provide the name of your Bitbucket account, preceded by the word bitbucket, as the `sonnen` parameter. E.g.,

`https://hawthornthistleberry.bitbucket.io/?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O&weatherKey=1n636n86o45p233qs98p5n23rqn1q58q&weatherCity=5233500&sonnen=bitbucketSusanBertram`

## The Sane Method

If Sonnen offers a real API similar to SolarEdge's, and you find out about it, by all that is holy in the world, send me the specs! I would sure love to replace the above hodgepodge of half-assed kluges with a modern, cloud-based approach. Or if you know anyone at Sonnen who actually answers their emails, ask them to consider giving us this. 

# Other Configuration Options

The remaining parameters are used to control things about the display to make it suit your preferences and needs. You'll see them listed in the Parameters section.

# Parameters

## SolarEdge Parameters

### code
**required; no default; six-digit number**

The site ID (typically a six-digit number) from SolarEdge for your site. You can see this on your SolarEdge monitor page.

### key
**required; no default; 32-character alphanumeric string**

Your API key from SolarEdge (a 32-character string of letters and numbers). Provided by SolarEdge or your installers.

### frequency
**optional; default=5; numeric**

How many minutes apart to do updates for the key data (current power level and historical data). The default, 5, is also the minimum, because any more often would exceed your limit of 300 API calls per day per API key. You might set this to a higher value if you want to save some calls; for instance, if you run this on two computers, set it to 10 to make updates go half as often on each. Whatever you choose, the graph will be updated 1/3 as often (default, every 15 minutes), and the environmental benefits will update 1/12 as often (default, once an hour). Note that weather and battery updates are not affected by this parameter, since there's no limit to the number of API calls you can make for those.

## Sonnen Battery Parameters

### sonnen
**optional; no default; an IP or DNS address**

A network address (IP or DNS) by which your Sonnen battery can be reached on port 8080 by the outside world. See the Sonnen Battery section for notes about the necessary network setup to make this work.

### cors
**optional; no default; a grida.co API key**

An API key provided by https://grida.co/ for use with their Bridged CORS proxy. See the Sonnen Battery section for notes about getting this key.

## Weather Parameters

### weatherKey
**optional; no default; long alphanumeric string**

Your API key from openweathermap.org. See Weather above for instructions. Note that if you have `weatherKey` you must also have `weatherCity` (and vice versa).

### weatherCity
**optional; no default; numeric ID or text name**

The city that is nearest you. You can put the name of the city, which may work, but many cities have non-unique names, and you can't use a state or country to distinguish. You can do a search at https://openweathermap.org/find to see if your city name is unique (at least within OpenWeatherMap), but to find the city ID, you have to go into their city list at http://bulk.openweathermap.org/sample/city.list.json.gz and compare latitude and longitudes. For instance, in Vermont, `weatherCity=5233500` is Barre, while `weatherCity=4951788` is Springfield.

## Display Parameters

### graphDays
**optional; default=5; numeric (1-30)**

Specify how many days of data to show in the graph.

### graphMax
**optional; no default; numeric**

If specified, the graph will use this as a number of watts to set as the top of the graph. You might set this to the maximum amount your power cells can produce, so that the graph always shows performance out of possible performance. If this is omitted, the graph will automatically scale to the highest value present, which means if you just had a few days of clouds, the graph will still suggest you had good production, at a glance, if you don't examine the axis. Note that if you ever produce more than the `graphMax` you specify, the graph will rescale anyway to show the data, rather than cutting it off. Also note that the graph scale is pinned to 1000 (1kW), so the max will actually adjust to the next highest 1000.

### value
**optional; no default; numeric (number of pennies)**

If provided, should indicate the value of 1kWh of energy in your area. Used to display a monetary value on all the energy displays. This value must by necessity be approximate since it's probably not true that every kWh has the same value as every other, so put in an average value. If omitted, dollar amounts aren't displayed.

### valueSymbol
**optional; default='$'; single character**

If provided, should indicate the symbol used in displaying monetary values. The values `dollar`, `pound`, `yen`, and `euro` will be replaced with the appropriate symbol. You can change the symbol, but the calculation still assumes the value represents 1/100th of the unit, as pennies to dollars, and rounds to a hundredth. If this gets much international use maybe I'll rethink this and make it more internationalized, but I doubt that'll come up. Heck, I doubt even this will get used.

### zoom
**optional; default=1; numeric (0.01 and up)**

If provided, causes the entire display to be scaled accordingly, which is useful if displaying on devices of different sizes. Default is 1. `zoom=0.5` halves the size, `zoom=2` doubles it, etc.

### black
**optional; default=false; true/false**

If set to `true`, a black background is used for the display.

### metric
**optional; default=false; true/false**

If set to `true`, CO2 saved is displayed in kilograms instead of pounds. If weather is configured, temperatures are displayed in metric units (Celsius) instead of Imperial (Fahrenheit). 

# Known Issues

- None.

# Revision History

- v1.0.0 2018-10-11 - Initial release with SolarEdge, OpenWeatherMap, Sonnen support.
- v1.1.0 2018-10-11 - Better error handling. metric option. valueSymbol option.
- v1.2.0 2018-10-16 - If weather is configured, use actual sunrise and sunset instead of 4:20am/8:40am, to save SolarEdge API calls.
- v1.3.0 2018-10-19 - At midnight, reset the graph and "so far today" counts since there's a new "today"
- v1.4.0 2018-10-21 - Reduced point size on graph. Battery charging/discharging are now color-coded.
- v1.5.0 2018-10-39 - New `graphMax` function lets you pin the scale of the graph.
- v1.6.0 2018-11-13 - Power arrows now change colors to suggest amount of power flow.
- v1.6.1 2018-11-27 - After a power outage let me test the Sonnen functions, I made some tweaks, but will have to wait for the next power outage to do further testing, so expect more changes here. No effect on those not using the Sonnen function.
- v1.6.2 2018-11-28 - More tweaks to Sonnen support based on working around inaccuracies in the API documentation.
- v1.6.3 2018-11-30 - Still more tweaks now that power is back up and my Sonnen is charging from the grid.
- v1.6.5 2018-12-02 - Changed CORS proxy and rewrote Sonnen call to use JSON instead of JSONP accordingly. Should be more reliable now.
- v1.6.6 2018-12-02 - Better handling when SolarEdge runs out and Sonnen is still active, without showing a NaN.
- v1.6.7 2018-12-04 - Another try to interpret what Sonnen means by production, consumption, and grid feed-in.
- v1.6.8 2019-09-03 - The CORS proxy I was using vanished; this update switches to another one, again. Back to JSONP too.
- v1.6.9 2019-09-04 - I got whateverorigin working and only a few hours later its certificate expired. Fixed their URL to not mind.
- v1.7.0 2020-03-01 - Yet another CORS proxy failed, but I was able to get cors-anywhere, one of those most stable, to work finally. Hopefully this will last.
- v1.7.1 2020-08-02 - The API returns CO2 in kg now, so I had to reverse the metric conversion to show kg or lb.
- v1.7.4 2021-05-29 - Using another new CORS proxy!
- v1.7.5 2022-03-02 - Now supporting https://grida.co/'s CORS proxy via an API key.

# Future Plans

- I'd like to add support for SolarEdge batteries instead of Sonnen batteries, but I have no access to any to test, so this seems impossible.
- Sonnen's API continues to cause problems due to CORS and routing. I suspect Sonnen has a better API but I can't find it.
- It'd be great to have a better way around CORS problems with Sonnen.

# Terms Of Use

This software is provided under the Creative Commons - Attribution license found at https://creativecommons.org/licenses/by/3.0/. In short, feel free to use it for personal use, to make alterations in your own copy, or even to distribute your altered versions (for free or commercially); just be sure that, if you distribute this or an altered version of this, you give credit to the original author (me) and in doing so, to the earlier authors whose work I used and am giving credit to (notably, but not exclusively, Jbithell on github; more are found in the code comments). People deserve credit for their work, and all the more so if they're sharing that work for free.

If you use this commercially, and charge for it, I would be grateful if you would allocate a percentage of the resulting revenue to a charity. You don't need to explicitly make it in my name. Some good choices are the Nature Conservancy, the Vermont Foodbank (or your local equivalent -- especially if you're a small local business, it'd be good to support your community), or the Central Vermont Humane Association (again, or your local equivalent). I'm not requiring this, just encouraging it.
