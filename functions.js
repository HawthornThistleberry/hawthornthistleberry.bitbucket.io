//Get function
(function($) {
    $.QueryString = (function(paramsArray) {
        let params = {};

        for (let i = 0; i < paramsArray.length; ++i)
        {
            let param = paramsArray[i]
                .split('=', 2);

            if (param.length !== 2)
                continue;

            params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, " "));
        }

        return params;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

// rounding function
function round(value, precision) {
    //var multiplier = Math.pow(10, precision || 0);
    ///return Math.round(value * multiplier) / multiplier;
    return value.toFixed(precision)
}

// display a number of watt-hours with appropriate units
var displayWattHours = function(data) {
    if (data < 1001) {
        return round(data,0) + "<font size='-1'> Wh</font>";
    } else if (data > 1000000) {
        return round(data/1000000,2) + "<font size='-1'> MWh</font>";
    } else {
        return round(data/1000,1) + "<font size='-1'> kWh</font>";
    }
};

// display a number of watts with appropriate units
var displayWatts = function(data) {
    if (data < 1001) {
        return round(data,0) + "<font size='-1'> W</font>";
    } else if (data > 1000000) {
        return round(data/1000000,2) + "<font size='-1'> MW</font>";
    } else {
        return round(data/1000,1) + "<font size='-1'> kW</font>";
    }
};

// display a number of watt-hours with appropriate units, plus, if we have a monetary value, the dollar equivalent
var displayKWHandValue = function(power, value) {
	var s;
	s = displayWattHours(power);
	if (power > 0 && value > 0)
	{
		s += " (" + energyValueSymbol + round(power * value / 100000, 2) + ")";
	}
	return s;
}

// build the display of cells in the top bar where there's an image and two lines of text
var displayImageAndTwoLines = function(img, alt, line1, line2) {
	return "<img src='img/" + img + ".png' alt='" + alt + "'><br />" + line1 + "<br />" + line2;
}


// ===========================================================================================================
// powerIcon() : An icon suitable for a certain power flow amount
var powerIcon = function(power) {
	if (power <= 10) return "Power-None";
	if (power <= 1000) return "Power-Low";
	if (power <= 3000) return "Power-Moderate";
	return "Power-High";
}


// converts a string to title case
var toTitleCase = function(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}


var setLastUpdated = function(error) {
	if (error == "")
	{
		$("#lastupdated").html("As of:<br />" + moment().format('MMMM Do YYYY, hh:mm:ss a'));
	} else {
		$("#lastupdated").html("As of:<br />" + moment().format('MMMM Do YYYY, hh:mm:ss a') + "<br /><b><font color='red'>" + error + "</font></b>");
	}
}

// ===========================================================================================================
// isDaytime() : Determines whether or not it's daytime
var isDaytime = function(dateTime) {
	if (sunriseTime != 0 && sunsetTime != 0)
	{
		// if sunrise and sunset are known, use them; they will be known if we are configured for weather, and it's run at least once
		//console.log("Checking now " + now.getTime() + " against sunrise " + sunriseTime + " and sunset " + sunsetTime);
		if (dateTime.getTime() >= sunriseTime && dateTime.getTime() <= sunsetTime) return 1; else return 0;
	} else {
		// if not use fixed times 4:20am/8:40pm, which are generally earlier than dawn and dusk for most of the world most of the time
		var dawn = new Date(), dusk = new Date();
		dawn = dusk = dateTime;
		dawn.setHours(4,20,0);
		dusk.setHours(20,40,0);
		//console.log("Checking now " + now + " against dawn " + dawn + " and dusk " + dusk);
		if (dateTime >= dawn && dateTime <= dusk) return 1; else return 0;
	}
};
