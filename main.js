/**************************************************************************************************
**
**   SolarEdge Monitor
**
**   by Frank J. Perricone (hawthorn@foobox.com)
**
**   Originally based on https://github.com/Jbithell/SolarEdgeESign (yet almost entirely rewritten)
**   Other sources are cited throughout where used
**
*/

// Global variables from the get parameter string
var siteCode, apiKey, energyValue, energyValueSymbol, graphDays, weatherKey, weatherCity, frequency, sonnen, CORSkey, metric, graphMax;

// Global variables used to store partial weather data since it takes two calls
var weatherIcon, weatherDescription, weatherTemp, weatherLow, weatherHigh;

// Global variable to store the power production since it's altered by the Sonnen
var currentPowerPV = 0;

// Global variables for sunrise and sunset, set by OpenWeatherMap and used by main loop
var sunriseTime = 0, sunsetTime = 0;

// ===========================================================================================================
// getCurrentPower() : Gets frequently updated information: current power and historical energy
//                     Runs every 5 minutes from 4:20 to 20:40 (193 times per day)
//                     Information source: SolarEdge API (https://solaredge.com/sites/default/files/se_monitoring_api.pdf)
var getCurrentPower = function() {

	setLastUpdated("");
    var now = new Date();

	try {
		$.jsonp({
			"url": "https://monitoringapi.solaredge.com/site/" + siteCode + "/overview.json?api_key=" + apiKey + "&version=1.0.0&callback=?",
			"data": {},
			"success": function (data) {
				var thisData = data.overview;

				$("#currentEnergy").html(displayImageAndTwoLines("House", "Home", displayKWHandValue(thisData.lastDayData.energy, energyValue), "so far today"));

				var s;
				s = "Month: " + displayKWHandValue(thisData.lastMonthData.energy, energyValue) + "<br />";
				s += "Year: " + displayKWHandValue(thisData.lastYearData.energy, energyValue) + "<br />";
				s += "Life: " + displayKWHandValue(thisData.lifeTimeData.energy, energyValue) + "<br />";
				$("#history").html(s);

				currentPowerPV = thisData.currentPower.power;
				$("#currentPowerPV").html(displayImageAndTwoLines(powerIcon(currentPowerPV), "Power From PV", displayWatts(currentPowerPV), sonnen == "" ? "from PV" : "from inverter"));
				$("#currentPowerHome").html(displayImageAndTwoLines(powerIcon(currentPowerPV), "Power To Home", displayWatts(currentPowerPV), sonnen == "" ? "to home" : "pass-through"));
			},
			"error": function (d, msg) {
				if (msg == "error") msg = "unspecified (probably too many calls today)";
				setLastUpdated("SolarEdge Error: " + msg);
			}
		});
	} catch(err) {
		setLastUpdated("SolarEdge Error: " + err.message);
	}

};

// ===========================================================================================================
// getGraph() : Gets historical information for the current day, or up to 30 previous, in graph form
//              Runs every 15 minutes from 4:20 to 20:40 (65 times per day)
//              Information source: SolarEdge API (https://solaredge.com/sites/default/files/se_monitoring_api.pdf)
var getGraph = function() {

	var now = new Date();
    var daysAgo = new Date();
	daysAgo.setHours(0,0,0); // always start at midnight
	if (graphDays != 1) // back up that many days, less one (for the current day)
	{
		daysAgo.setTime(daysAgo.getTime() - (86400000 * (graphDays - 1)));
	}

	try {
		$.jsonp({
			"url": "https://monitoringapi.solaredge.com/site/" + siteCode + "/power.json?&endTime=" + $.datepicker.formatDate('yy-mm-dd', now) + " " + now.getHours() + ":" + now.getMinutes() + ":00" + "&startTime=" + $.datepicker.formatDate('yy-mm-dd', daysAgo) + " 00:00:00&api_key=" + apiKey + "&version=1.0.0&callback=?",
			"data": {},
			"success": function (data) {
				var times = [], values = [];
				$.each( data.power.values, function( index, value ){
					times.push(value.date);
					values.push(value.value);
				});
				var ctx = document.getElementById("powerGraph").getContext('2d');
				if (graphMax > 0)
				{
					Chart.scaleService.updateScaleDefaults('linear', {
						ticks: {
							suggestedMax: graphMax
						}
					});
				}
				var myChart = new Chart(ctx, {
					type: 'line',
					data: {
						labels: times,
						xValueType: "dateTime",
						label: "",
						datasets: [{
							data: values,
							fill: 'origin',
							pointRadius: 2,
							backgroundColor:"rgb(50, 205, 50)",
							borderColor:"rgb(0, 128, 0)",
							pointBorderColor:"rgb(0,0,205)",
							pointBackgroundColor:"rgb(30,144,255)",
						}]

					},
					options: {
						events: [], // disable all interactivity in the graph
						animation: {
							duration: 0, // Turn off animations as this is for a screen
						},
						hover: {
							animationDuration: 0, // Turn off animations as this is for a screen
							intersect: false,
						},
						responsiveAnimationDuration: 0, // Turn off animations as this is for a screen
						legend: {
							display: false
						},
						scales: {
							xAxes: [{
								type: 'time',
								position: 'bottom',
								time: {
									unit: (graphDays == 1 ? "hour" : "day"),
								},
							}],
							yAxes: [{
								scaleLabel: {
									display: true,
									labelString: 'Power (W)'
								},
								ticks: {
									//max: 8500,
									min: 0,
									precison: 3
								}
							}]
						},
					}
				});
				
			},
			"error": function (d, msg) {
				if (msg == "error") msg = "unspecified (probably too many calls today)";
				setLastUpdated("SolarEdge Error: " + msg);
			}
		});
	} catch(err) { 
		setLastUpdated("SolarEdge Error: " + err.message);
	}
};

// ===========================================================================================================
// getEnvironmentalBenefits() : Gets informational about environmental benefits, which don't need to be timely
//                              Runs every 60 minutes from 4:20 to 20:40 (17 times per day)
//                              Information source: SolarEdge API (https://solaredge.com/sites/default/files/se_monitoring_api.pdf)
var getEnvironmentalBenefits = function() {

	try {
		$.jsonp({
			"url": "https://monitoringapi.solaredge.com/site/" + siteCode + "/envBenefits.json?api_key=" + apiKey + "&version=1.0.0&callback=?",
			"data": {},
			"success": function (data) {
				var savedco2 = round(data.envBenefits.gasEmissionSaved.co2 * 2.2,2) + "<font size='-1'> lb</font>";
				if (metric) savedco2 = round(data.envBenefits.gasEmissionSaved.co2,2) + "<font size='-1'> kg</font>";

				$("#envbenefits").html("CO<sub>2</sub> Saved: " + savedco2 + "<br />Trees Planted: " + round(data.envBenefits.treesPlanted,2) + "<br />");
			},
			"error": function (d, msg) {
				if (msg == "error") msg = "unspecified (probably too many calls today)";
				setLastUpdated("SolarEdge Error: " + msg);
			}
		});
	} catch(err) { 
		setLastUpdated("SolarEdge Error: " + err.message);
	}
};


// ===========================================================================================================
// getWeather() : Gets the current weather (state, temperature, low, and high)
//                Runs every 5 minutes all day long (288 times per day, or 576 calls per day)
//                Information Source: OpenWeatherMap API (https://openweathermap.org/current)
var getWeather = function() {
	if (weatherKey == "" || weatherCity == "") return;

	var degreeSymbol = "&deg;F";
	var metricURL = "&units=imperial";
	if (metric == 1) {
		degreeSymbol = "&deg;C";
		metricURL = "&units=metric";
	}

	try {
		// first get current conditions
		$.jsonp({
			"url": "https://api.openweathermap.org/data/2.5/weather?appid=" + weatherKey + "&" + (isNaN(weatherCity) ? "q=" : "id=") + weatherCity + metricURL + "&callback=?",
			"data": {},
			"success": function (data) {
				weatherIcon = data.weather[0].icon.slice(0,-1);
				weatherDescription = toTitleCase(data.weather[0].description);
				weatherTemp = round(data.main.temp,0) + degreeSymbol;
				$("#currentWeather").html(displayImageAndTwoLines(weatherIcon, weatherDescription, weatherDescription, weatherTemp + " (" + weatherLow + "-" + weatherHigh + degreeSymbol + ")"));
				// save sunrise and sunset for use in the loop; this will change at midnight
				sunriseTime = data.sys.sunrise * 1000;
				sunsetTime = data.sys.sunset * 1000;
			},
			"error": function (d, msg) {
				if (msg == "error") msg = "unspecified";
				setLastUpdated("OpenWeather Error: " + msg);
			}
		});
	} catch(err) {
		setLastUpdated("OpenWeather Error: " + err.message);
	}

	try {
		// next get the forecast to get the daily low and high
		$.jsonp({
			"url": "https://api.openweathermap.org/data/2.5/forecast?appid=" + weatherKey + "&" + (isNaN(weatherCity) ? "q=" : "id=") + weatherCity + metricURL + "&callback=?",
			"data": {},
			"success": function (data) {
				var lowest = 99999, highest=0, count = data.cnt;
				for (count = 0; count < data.cnt; count++)
				{
					if (data.list[count].main.temp < lowest) lowest = data.list[count].main.temp;
					if (data.list[count].main.temp > highest) highest = data.list[count].main.temp;
				}
				weatherLow = round(lowest,0);
				weatherHigh = round(highest,0);
				$("#currentWeather").html(displayImageAndTwoLines(weatherIcon, weatherDescription, weatherDescription, weatherTemp + " (" + weatherLow + "-" + weatherHigh + degreeSymbol + ")"));
			},
			"error": function (d, msg) {
				if (msg == "error") msg = "unspecified";
				setLastUpdated("OpenWeather Error: " + msg);
			}
		});
	} catch(err) { 
		setLastUpdated("OpenWeather Error: " + err.message);
	}
};

// ===========================================================================================================
// getSonnen() : Gets the Sonnen battery status
//               Runs every 10 minutes all day long (144 calls per day)
//               Information Source: Sonnen API (https://github.com/foxriver76/ioBroker.sonnen/blob/master/docs/sonnen_rest_api.pdf)
var getSonnen = function() {
	if (sonnen == "") return;

	var url, fetchOptions;
	if (sonnen.startsWith('bitbucket')) {
		url = 'https://bitbucket.org/' + sonnen.substring(9) + '/sonnenstatus/raw/master/SonnenStatus.json';
		fetchOptions = null;
	} else {
		url = 'https://cors.bridged.cc/http://' + sonnen + ':8080/api/v1/status';
		fetchOptions = {
			headers: {
			  'x-cors-grida-api-key': CORSkey
			}
		}
	}

	try {
		fetch(url, fetchOptions)
		  .then((response) => {
			//console.log(response);
			return response.json();
		  })
		  .then((data) => {
			//console.log(data);
			if (data === null)
			{
				setLastUpdated("Sonnen Error: could not read from battery");
				return;
			}
			if (data.contents != undefined) data = JSON.parse(data.contents);

			/* Note: contrary to the PDF, this is what I actually saw, without the key Pac_total_W value present at all, during a power outage:
			
			{"BackupBuffer":"97","BatteryCharging":false,"BatteryDischarging":false,"Consumption_W":46,"Fac":60,"FlowConsumptionBattery":false,"FlowConsumptionGrid":false,"FlowConsumptionProduction":true,"FlowGridBattery":false,"FlowProductionBattery":false,"FlowProductionGrid":true,"GridFeedIn_W":5252,"IsSystemInstalled":1,"OperatingMode":"7","Pac_total_W":0,"Production_W":5298,"RSOC":100,"SystemStatus":"OnGrid","Timestamp":"2021-03-25 14:53:14","USOC":100,"Uac":249,"Ubat":54}

			With power out:

			{"BackupBuffer":"97","BatteryCharging":false,"BatteryDischarging":true,"Consumption_W":1789,"Fac":0,"FlowConsumptionBattery":true,"FlowConsumptionGrid":false,"FlowConsumptionProduction":true,"FlowGridBattery":false,"FlowProductionBattery":false,"FlowProductionGrid":false,"GridFeedIn_W":0,"IsSystemInstalled":1,"OperatingMode":"7","Pac_total_W":1500,"Production_W":26,"RSOC":99,"SystemStatus":"OffGrid","Timestamp":"2021-03-26 14:41:53","USOC":99,"Uac":0,"Ubat":53}
			
			Still not totally sure I am correctly interpreting what GridFeedIn_W, Consumption_W, and Production_W represent, but have done some comparisons of values I see with what the Sonnen web page is showing me at the same moment, to get the following stab at some logic.
			*/

			// find battery icon nearest to the value
			var batteryIcon = "Battery-" + round(data.USOC / 10, 0) * 10;

			// determine battery status
			var batteryStatus = "idle";
			if (data.BatteryCharging) {
				batteryStatus = "<font color='green'>charging " + displayWatts(data.Production_W + data.GridFeedIn_W) + "</font>";
			} else if (data.BatteryDischarging) {
				batteryStatus = "<font color='red'>discharge " + displayWatts(data.Consumption_W) + "</font>";
			}

			$("#currentBattery").html(displayImageAndTwoLines(batteryIcon, "Battery " + data.USOC + "%", data.USOC + "%", batteryStatus));

			// update passthrough by subtracting battery consumption from the power from PV
			var displayPower = currentPowerPV + (data.Production_W < 0 ? data.Production_W : 0);
			if (displayPower < 0) displayPower = 0;
			$("#currentPowerHome").html(displayImageAndTwoLines(powerIcon(displayPower), "Power To Home", displayWatts(displayPower), "pass-through"));
		})
		.catch((msg, d) => {
			console.log("Sonnen call error " + msg + ": " + JSON.stringify(d));
			if (msg == "error") msg = "unspecified";
			setLastUpdated("Sonnen Call Error: " + msg);
		  });

	} catch(err) { 
		setLastUpdated("Sonnen Catch Error: " + err.message);
	}

};


// ===========================================================================================================
// Page main function, runs at page load
$( document ).ready(function() {

	// set the page zoom
    if (typeof $.QueryString["zoom"] !== 'undefined' && isNaN($.QueryString["zoom"]) !== true) {
        //Set page zoom
        if ($.QueryString["zoom"] > 1 || $.QueryString["zoom"] < 0.01) {
            var scale = 'scale(1)';
        } else {
            var scale = 'scale(' + $.QueryString["zoom"] + ')';
        }
        document.body.style.webkitTransform = scale;  // Chrome, Opera, Safari
        document.body.style.msTransform = scale;      // IE 9
        document.body.style.transform = scale;        // General
    }

	// set a black background if specified
    if (typeof $.QueryString["black"] !== 'undefined' && $.QueryString["black"] == "true") {
        // Black background mode
        $('body').css('background-color', 'black');
        $('body').css('color', 'white');
    }

	// capture the frequency multiplier for checks
    if (typeof $.QueryString["frequency"] !== 'undefined' && isNaN($.QueryString["frequency"]) !== true) {
        if ($.QueryString["frequency"] < 5) {
            frequency = 5;
        } else {
            frequency = $.QueryString["frequency"];
        }
    } else frequency = 5;

	// capture the monetary value of kilowatt-hours at your site, if provided
    if (typeof $.QueryString["value"] !== 'undefined') {
		energyValue = $.QueryString["value"];
    } else {
		energyValue = 0;
	}

	// capture the monetary symbol, if provided
    if (typeof $.QueryString["valueSymbol"] !== 'undefined') {
		energyValueSymbol = $.QueryString["valueSymbol"];
    } else {
		energyValueSymbol = "$";
	}
	if (energyValueSymbol == "pound") energyValueSymbol = "&pound;";
	if (energyValueSymbol == "dollar") energyValueSymbol = "&dollar;";
	if (energyValueSymbol == "euro") energyValueSymbol = "&euro;";
	if (energyValueSymbol == "yen") energyValueSymbol = "&yen;";

	// capture the sonnen thingamabob (not sure if we can do this yet so not sure what it should be so for now it's just that it has a value)
    if (typeof $.QueryString["sonnen"] !== 'undefined') {
		sonnen = $.QueryString["sonnen"];
		// for now just change it to show the battery icon and some placeholders
		$("#currentBattery").html("<img src='img/Battery-100.png' alt='Sonnen Battery'><br />??%<br />status unknown");
    } else {
		sonnen = "";
	}

	// capture the CORS key
    if (typeof $.QueryString["cors"] !== 'undefined') {
		CORSkey = $.QueryString["cors"];
    } else {
		CORSkey = "";
	}

	// capture the desired number of days for the graph
    if (typeof $.QueryString["graphDays"] !== 'undefined') {
		graphDays = $.QueryString["graphDays"];
    } else {
		graphDays = 5;
	}
	if (graphDays > 30) graphDays = 30;

	// capture the desired number of days for the graph
    if (typeof $.QueryString["graphMax"] !== 'undefined') {
		graphMax = $.QueryString["graphMax"];
    } else {
		graphMax = 0;
	}

	// capture the weather city and key
    if (typeof $.QueryString["weatherKey"] !== 'undefined') {
		weatherKey = $.QueryString["weatherKey"];
    } else {
		weatherKey = "";
	}
    if (typeof $.QueryString["weatherCity"] !== 'undefined') {
		weatherCity = $.QueryString["weatherCity"];
    } else {
		weatherCity = "";
	}

	// set weather data to starting defaults
	weatherIcon = "SolarCells";
	weatherDescription = "Unknown";
	weatherTemp = "?&deg;F";
	weatherLow = "?&deg;F";
	weatherHigh = "?&deg;F";

	// choose metric units
    if (typeof $.QueryString["metric"] !== 'undefined' && $.QueryString["metric"] == "true") {
        metric = 1;
		weatherTemp = "?&deg;C";
		weatherLow = "?&deg;C";
		weatherHigh = "?&deg;C";
    } else metric = 0;


	// capture the necessary code and key
    siteCode = $.QueryString["code"];
    apiKey = $.QueryString["key"];

	// validate the required information
    if (typeof siteCode === 'undefined' || !siteCode || typeof apiKey === 'undefined' || !apiKey || siteCode.length <6 || apiKey.length <1 ) {
        bootbox.dialog({
            message: '<p class="text-center">Please pass a code and API key in the URL in this format:<br />https://your.site/index.html?code=123456&key=L4QLVQ1LOKCQX2193VSEICXW61NP6B1O</p>',
            closeButton: false
        });
    } else {
		// initially load all data on the page
		getCurrentPower();
		getEnvironmentalBenefits();
		getGraph();
		getWeather();
		getSonnen();

		// set up the timer loop for SolarEdge data
        setInterval(function() {
			var now = new Date();
			var minutesSinceMidnight = (now.getHours() * 60) + now.getMinutes();
			if (isDaytime(now))
			{
				// if the time is an even multiple of five minutes, run getCurrentPower()
				if (minutesSinceMidnight % frequency == 0)
				{
					getCurrentPower(); // this also handles the last-updated date
				}
				// if the time is an even multiple of fifteen minutes, run getGraph()
				if (minutesSinceMidnight % (3*frequency) == 0)
				{
					getGraph();
				}
				// if the time is an even multiple of an hour, run getEnvironmentalBenefits()
				if (minutesSinceMidnight % (12*frequency) == 0)
				{
					getEnvironmentalBenefits();
				}
			} else if (minutesSinceMidnight <= 0) {
				// at midnight, reset some things that refer to "today"
				$("#currentEnergy").html(displayImageAndTwoLines("House", "Home", displayKWHandValue(0, energyValue), "so far today"));
				getGraph();
			}
        }, 1000 * 60 * 1); // run this loop every minute

		// set up the timer loops for weather and Sonnen, which are not limited by dawn/dusk
        setInterval(function() {
			getWeather();
        }, 1000 * 60 * 5); // run this loop every five minutes
        setInterval(function() {
			getSonnen();
        }, 1000 * 60 * 10); // run this loop every ten minutes
    }

});

